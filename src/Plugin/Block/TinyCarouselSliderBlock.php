<?php

namespace Drupal\tiny_carousel_slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'TinyCarouselSlider' Block.
 *
 * @Block(
 * id = "tiny_carousel_slider",
 * admin_label = @Translation("Tiny Carousel Slider"),
 * )
 */
class TinyCarouselSliderBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['gal_width'])) {
      $gal_width = $config['gal_width'];
    }
    else {
      $gal_width = "";
    }

    if (!empty($config['gal_height'])) {
      $gal_height = $config['gal_height'];
    }
    else {
      $gal_height = "";
    }

    if (!empty($config['gal_controls'])) {
      $gal_controls = $config['gal_controls'];
      if ($gal_controls != "true" && $gal_controls != "false") {
        $gal_controls = "true";
      }
    }
    else {
      $gal_controls = "true";
    }

    if (!empty($config['gal_autointerval'])) {
      $gal_autointerval = $config['gal_autointerval'];
      if ($gal_autointerval != "true" && $gal_autointerval != "false") {
        $gal_autointerval = "true";
      }
    }
    else {
      $gal_autointerval = "true";
    }

    if (!empty($config['gal_intervaltime'])) {
      $gal_intervaltime = $config['gal_intervaltime'];
      if (!is_numeric($gal_intervaltime)) {
        $gal_intervaltime = 1500;
      }
    }
    else {
      $gal_intervaltime = "1500";
    }

    if (!empty($config['gal_animation'])) {
      $gal_animation = $config['gal_animation'];
      if (!is_numeric($gal_animation)) {
        $gal_animation = 1000;
      }
    }
    else {
      $gal_animation = "1000";
    }

    if (!empty($config['gal_random'])) {
      $gal_random = $config['gal_random'];
      if ($gal_random == "true") {
        $gal_random = "YES";
      }
      elseif ($gal_random == "false") {
        $gal_random = "NO";
      }
      else {
        $gal_random = "NO";
      }
    }
    else {
      $gal_random = "NO";
    }

    if (!empty($config['gal_imagedetails'])) {
      $gal_imagedetails = $config['gal_imagedetails'];
    }
    else {
      $gal_imagedetails = "";
    }

    // 5 minutes, 5 * 60 = 300.
    $output[]['#cache']['max-age'] = 0;
    $values = [
      'gal_width' => $gal_width,
      'gal_height' => $gal_height,
      'gal_controls' => $gal_controls,
      'gal_autointerval' => $gal_autointerval,
      'gal_intervaltime' => $gal_intervaltime,
      'gal_animation' => $gal_animation,
      'gal_random' => $gal_random,
      'gal_imagedetails' => $gal_imagedetails,
    ];

    $markup = $this->tinyBlock($values);
    $output[] = [
      '#markup' => Markup::create($markup),
      '#allowed_tags' => ['script', 'div', 'a', 'style', 'img'],
    ];
    $output['#attached']['library'][] = 'tiny_carousel_slider/tiny_carousel_slider';

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  private function tinyBlock(array $values) {

    $imageli = "";
    $tch = "";
    $gal_width = $values["gal_width"];
    $gal_height = $values["gal_height"];
    $gal_controls = $values["gal_controls"];
    $gal_autointerval = $values["gal_autointerval"];
    $gal_intervaltime = $values["gal_intervaltime"];
    $gal_animation = $values["gal_animation"];
    $gal_random = $values["gal_random"];
    $gal_imagedetails = $values["gal_imagedetails"];
    $gal_width1 = $gal_width + 4;
    $gal_height1 = $gal_height + 4;

    $gal_imagedetails_Ar = explode("\n", $gal_imagedetails);
    $gal_imagedetails_Ar = array_filter($gal_imagedetails_Ar, 'trim');

    if ($gal_random == "YES") {
      shuffle($gal_imagedetails_Ar);
    }

    if (count($gal_imagedetails_Ar) > 0) {
      foreach ($gal_imagedetails_Ar as $gal_imagedetail) {
        $gal_imagedetail = preg_replace("/\r|\n/", "", $gal_imagedetail);

        if (strpos($gal_imagedetail, ']-[') !== FALSE) {
          $gal_imagedetail = explode("]-[", $gal_imagedetail);
          $gal_image = $gal_imagedetail[0];
          $gal_link = $gal_imagedetail[1];
        }
        elseif (strpos($gal_imagedetail, '] - [') !== FALSE) {
          $gal_imagedetail = explode("] - [", $gal_imagedetail);
          $gal_image = $gal_imagedetail[0];
          $gal_link = $gal_imagedetail[1];
        }
        elseif (strpos($gal_imagedetail, ']- [') !== FALSE) {
          $gal_imagedetail = explode("]- [", $gal_imagedetail);
          $gal_image = $gal_imagedetail[0];
          $gal_link = $gal_imagedetail[1];
        }
        elseif (strpos($gal_imagedetail, '] -[') !== FALSE) {
          $gal_imagedetail = explode("] -[", $gal_imagedetail);
          $gal_image = $gal_imagedetail[0];
          $gal_link = $gal_imagedetail[1];
        }
        else {
          if ($gal_imagedetail <> "") {
            $gal_image = $gal_imagedetail;
            $gal_link = "#";
          }
        }

        $gal_image = ltrim($gal_image, '[');
        $gal_image = rtrim($gal_image, ']');
        $gal_link = ltrim($gal_link, '[');
        $gal_link = rtrim($gal_link, ']');

        $imageli = $imageli . '<li>';
        $imageli = $imageli . '<a href="' . $gal_link . '" target="_target">';
        $imageli = $imageli . '<img alt="" src="' . $gal_image . '" />';
        $imageli = $imageli . '</a>';
        $imageli = $imageli . '</li>';
      }

      if ($imageli <> "") {
        $tch = $tch . "<style type='text/css' media='screen'>
        #tchsp { height: 1%; margin: 30px 0 0; overflow:hidden; position: relative; padding: 0 50px 10px;   }
        #tchsp .viewport { height: " . $gal_height1 . "px; overflow: hidden; position: relative; }
        #tchsp .buttons { background: #C01313; border-radius: 35px; display: block; position: absolute;
        top: 40%; left: 0; width: 35px; height: 35px; color: #fff; font-weight: bold; text-align: center; line-height: 35px; text-decoration: none;
        font-size: 22px; }
        #tchsp .next { right: 0; left: auto;top: 40%; }
        #tchsp .buttons:hover{ color: #C01313;background: #fff; }
        #tchsp .disable { visibility: hidden; }
        #tchsp .overview { list-style: none; position: absolute; padding: 0; margin: 0; width: " . $gal_width1 . "px; left: 0 top: 0; }
        #tchsp .overview li{ float: left; margin: 0 20px 0 0; padding: 1px; height: " . $gal_height . "px; border: 1px solid #dcdcdc; width: " . $gal_width . "px;}
        </style>";

        $tch = $tch . '<div id="tchsp">';
        if ($gal_controls == 'true') {
          $tch = $tch . '<a class="buttons prev" href="#">&#60;</a>';
        }

        $tch = $tch . '<div class="viewport">';
        $tch = $tch . '<ul class="overview">';
        $tch = $tch . $imageli;
        $tch = $tch . '</ul>';
        $tch = $tch . '</div>';

        if ($gal_controls == 'true') {
          $tch = $tch . '<a class="buttons next" href="#">&#62;</a>';
        }
        $tch = $tch . '</div>';

        $tch = $tch . '<script type="text/javascript">';
        $tch = $tch . 'jQuery(document).ready(function(){';
        $tch = $tch . "jQuery('#tchsp').tinycarousel({ buttons: " . $gal_controls . ", interval: " . $gal_autointerval . ", intervalTime: " . $gal_intervaltime . ", animationTime: " . $gal_animation . " });";
        $tch = $tch . '});';
        $tch = $tch . '</script>';
      }
    }
    return $tch;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['gal_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter width'),
      '#description' => $this->t('Enter your image width, You should add same size (width) images in this gallery. (Ex: 100)'),
      '#default_value' => isset($config['gal_width']) ? $config['gal_width'] : '100',
    ];

    $form['gal_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter height'),
      '#description' => $this->t('Enter your image height, You should add same size (height) images in this gallery. (Ex: 75)'),
      '#default_value' => isset($config['gal_height']) ? $config['gal_height'] : '75',
    ];

    $options_YESNO = [
      'true' => $this->t('YES'),
      'false' => $this->t('NO'),
    ];

    $form['gal_controls'] = [
      '#type' => 'radios',
      '#title' => $this->t('Controls'),
      '#options' => $options_YESNO,
      '#description' => $this->t('Need to use the Left, Right arrow button in your gallery?'),
      '#default_value' => isset($config['gal_controls']) ? $config['gal_controls'] : 'true',
    ];

    $form['gal_autointerval'] = [
      '#type' => 'radios',
      '#title' => $this->t('Auto interval'),
      '#options' => $options_YESNO,
      '#description' => $this->t('Need to add auto interval to move one image from another?'),
      '#default_value' => isset($config['gal_autointerval']) ? $config['gal_autointerval'] : 'true',
    ];

    $form['gal_intervaltime'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interval time'),
      '#description' => $this->t('Enter auto interval time in millisecond. (Ex: 1500)'),
      '#default_value' => isset($config['gal_intervaltime']) ? $config['gal_intervaltime'] : '1500',
    ];

    $form['gal_animation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Animation time'),
      '#description' => $this->t('Enter animation duration in millisecond. (Ex: 1000)'),
      '#default_value' => isset($config['gal_animation']) ? $config['gal_animation'] : '1000',
    ];

    $form['gal_random'] = [
      '#type' => 'radios',
      '#title' => $this->t('Random display'),
      '#options' => $options_YESNO,
      '#description' => $this->t('Do you want to display images in random order?'),
      '#default_value' => isset($config['gal_random']) ? $config['gal_random'] : 'true',
    ];

    $form['gal_imagedetails'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter image details'),
      '#description' => $this->t('Please enter image url. One image url per line. [Where is the picture located on the internet]-[When someone clicks on the picture, where do you want to send them] <br /> Example : [http://www.gopiplus.com/work/wp-content/uploads/pluginimages/250x167/250x167_1.jpg]-[http://www.gopiplus.com/]'),
      '#default_value' => isset($config['gal_imagedetails']) ? $config['gal_imagedetails'] : '[http://www.gopiplus.com/work/wp-content/uploads/pluginimages/100x75/sing_1.jpg]-[http://www.gopiplus.com/]',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['gal_width'] = $form_state->getValue('gal_width');
    $this->configuration['gal_height'] = $form_state->getValue('gal_height');
    $this->configuration['gal_controls'] = $form_state->getValue('gal_controls');
    $this->configuration['gal_autointerval'] = $form_state->getValue('gal_autointerval');
    $this->configuration['gal_intervaltime'] = $form_state->getValue('gal_intervaltime');
    $this->configuration['gal_animation'] = $form_state->getValue('gal_animation');
    $this->configuration['gal_random'] = $form_state->getValue('gal_random');
    $this->configuration['gal_imagedetails'] = $form_state->getValue('gal_imagedetails');
  }

}
