CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainersss


INTRODUCTION
------------

This is lightweight carousel image sliding drupal module. 
This module is using Tiny Carousel JavaScript jQuery library. 
It is a lightweight module that gives you excellent user 
interface and fast page loading. Sliding interval can be set 
to slide automatically every given milliseconds. Sliding 
animation time can be set in milliseconds.


FEATURES:
---------

1. Easy to customize.
2. Support all browser.
3. Automatically pauses on mouse over.
4. Supports navigation by button.
5. Responsive


REQUIREMENTS
------------

Just Drupal module Tiny Carousel Slider files


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
Link: https://www.drupal.org/documentation/install/
modules-themes/modules-8


CONFIGURATION
-------------

1. Go to Admin >> Extend page and check the Enabled 
check box near to the module Tiny Carousel Slider 
and then click the Install button at the bottom.

2. Go to Admin >> Structure >> Block layout page, 
there you can see button called Place Block near 
each available blocks.

3. Go to Admin >> Structure >> Blocks layout page 
and click Place Block button to add Block. If you want 
to add Block in your Breadcrumb, click Place Block button 
near your Breadcrumb title. It will open Place block window. 
In that window again click Place Block button near Tiny Carousel Slider.

4. Now open your website front end and see 
Tiny Carousel Slider module in the selected 
location. go to configuration link of the 
module and update the default text message.


MAINTAINERS
-----------

Current maintainers:

 * Gopi RAMASAMY 
https://www.drupal.org/user/1388160
http://www.gopiplus.com/work/about/
http://www.gopiplus.com/extensions/2017/02/drupal-module-tiny-carousel-slider/
 
Requires - Drupal 8
License - GPL (see LICENSE)
