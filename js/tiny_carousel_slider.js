(function (factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(jQuery || ['jquery'], factory);
  }
  else if (typeof exports === 'object') {
    factory(jQuery || require('jquery'));
  }
  else {
    factory(jQuery);
  }
}(function ($) {
  'use strict';
  var pluginName = 'tinycarousel';
  var defaults =
    {
      start: 0,
      axis: 'x',
      buttons: true,
      bullets: false,
      interval: false,
      intervalTime: 3000,
      animation: true,
      animationTime: 1000,
      infinite: true
    };

  function Plugin($container, options) {
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;

    var self = this;
    var $viewport = $container.find('.viewport:first');
    var $overview = $container.find('.overview:first');
    var $slides = 0;
    var $next = $container.find('.next:first');
    var $prev = $container.find('.prev:first');
    var $bullets = $container.find('.bullet');

    var viewportSize = 0;
    var contentStyle = {};
    var slidesVisible = 0;
    var slideSize = 0;
    var slideIndex = 0;

    var isHorizontal = this.options.axis === 'x';
    var sizeLabel = isHorizontal ? 'Width' : 'Height';
    var posiLabel = isHorizontal ? 'left' : 'top';
    var intervalTimer = null;

    // var slideCurrent;
    this.slideCurrent = 0;
    this.slidesTotal = 0;

    function initialize() {
      self.update();
      self.move(self.slideCurrent);

      setEvents();

      return self;
    }

    this.update = function () {
      $overview.find('.mirrored').remove();

      $slides = $overview.children();
      viewportSize = $viewport[0]['offset' + sizeLabel];
      slideSize = $slides.first()['outer' + sizeLabel](true);
      self.slidesTotal = $slides.length;
      // slideCurrent = self.options.start || 0;
      slidesVisible = Math.ceil(viewportSize / slideSize);

      $overview.append($slides.slice(0, slidesVisible).clone().addClass('mirrored'));
      $overview.css(sizeLabel.toLowerCase(), slideSize * (self.slidesTotal + slidesVisible));

      return self;
    };

    function setEvents() {
      if (self.options.buttons) {
        $prev.click(function () {
          self.move(--slideIndex);

          return false;
        });

        $next.click(function () {
          self.move(++slideIndex);

          return false;
        });
      }

      $(window).resize(self.update);

      if (self.options.bullets) {
        $container.on('click', '.bullet', function () {
          self.move(slideIndex = +$(this).attr('data-slide'));

          return false;
        });
      }
    }

    this.start = function () {
      if (self.options.interval) {
        clearTimeout(intervalTimer);

        intervalTimer = setTimeout(function () {
          self.move(++slideIndex);

        }, self.options.intervalTime);
      }

      return self;
    };

    this.stop = function () {
      clearTimeout(intervalTimer);

      return self;
    };

    this.move = function (index) {
      slideIndex = index;
      self.slideCurrent = slideIndex % self.slidesTotal;

      if (slideIndex < 0) {
        self.slideCurrent = slideIndex = self.slidesTotal - 1;
        $overview.css(posiLabel, -(self.slidesTotal) * slideSize);
      }

      if (slideIndex > self.slidesTotal) {
        self.slideCurrent = slideIndex = 1;
        $overview.css(posiLabel, 0);
      }

      contentStyle[posiLabel] = -slideIndex * slideSize;

      $overview.animate(
        contentStyle,
        {
          queue: false,
          duration: self.options.animation ? self.options.animationTime : 0,
          always: function () {
            $container.trigger('move', [$slides[self.slideCurrent], self.slideCurrent]);
          }
        });

      setButtons();
      self.start();

      return self;
    };

    function setButtons() {
      if (self.options.buttons && !self.options.infinite) {
        $prev.toggleClass('disable', self.slideCurrent <= 0);
        $next.toggleClass('disable', self.slideCurrent >= self.slidesTotal - slidesVisible);
      }

      if (self.options.bullets) {
        $bullets.removeClass('active');
        $($bullets[self.slideCurrent]).addClass('active');
      }
    }

    return initialize();
  }

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, 'plugin_' + pluginName)) {
        $.data(this, 'plugin_' + pluginName, new Plugin($(this), options));
      }
    });
  };
}));
